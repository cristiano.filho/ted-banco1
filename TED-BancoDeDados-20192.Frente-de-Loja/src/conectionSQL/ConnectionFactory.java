package conectionSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	static String url = "jdbc:postgresql://localhost:5432/postgres";
	static String user = "postgres";
	static String password = "123";

	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(url, user, password);
			con.createStatement().execute("CREATE TABLE IF NOT EXISTS dono (\r\n" + "	nome varchar(255) NOT NULL,\r\n"
					+ "	email varchar(255) NOT NULL,\r\n" + "	senha varchar(20) NOT NULL);\r\n"
					+ "ALTER TABLE dono ADD CONSTRAINT pk_emailUsuario PRIMARY KEY(email);\r\n" + "\r\n"
					+ "CREATE TABLE IF NOT EXISTS categoria	(\r\n" + "	Id_Categoria serial NOT NULL,\r\n"
					+ "	codigo_categoria integer NOT NULL,\r\n" + "	descricao varchar(255),\r\n"
					+ "	data_inicial date,\r\n" + "	email_Usuario varchar(255) NOT NULL);\r\n" + "\r\n"
					+ "ALTER TABLE estoque ADD CONSTRAINT id_fk_fornecedor\r\n" + "FOREING KEY(nome_contato)\r\n"
					+ "REFERENCE  fornecedor(id); \r\n" + "\r\n" + "ALTER TABLE pedido ADD CONSTRAINT id_fk_dono \r\n"
					+ "FOREIGN KEY(nome)\r\n" + "REFERENCE dono(id); \r\n" + "\r\n"
					+ "ALTER TABLE itempedido ADD CONSTRAINT id_fk_pedido\r\n" + "FOREING KEY(id_pedido)\r\n"
					+ "REFERENCE pedido(id); \r\n" + "\r\n" + "ALTER TABLE fornecedor ADD CONSTRAINT id_fk_produto \r\n"
					+ "FOREING KEY(nome)\r\n" + "REFERENCE produto(id); \r\n" + "\r\n"
					+ "ALTER TABLE login ADD CONSTRAINT id_fk_dono\r\n" + "FOREING KEY(nome)\r\n"
					+ "REFERENCE dono(id); \r\n" + "\r\n" + "ALTER TABLE produto ADD CONSTRAINT id_fk_categoria \r\n"
					+ "FOREING KEY(id_categoria)\r\n" + "REFERENCE categoria(id); \r\n" + "\r\n"
					+ "ALTER TABLE ADD CONSTRAINT id_fk_produto\r\n" + "FOREING KEY(id_produto)\r\n"
					+ "REFENRECE produto(id); \r\n");
			System.out.println("Conectado com sucesso");
		} catch (SQLException e) {
			System.out.println("Erro - Conexao " + e.getMessage());
		} catch (ClassNotFoundException e) {
			System.out.println("Erro - Driver " + e.getMessage());
		}
		return con;
	}

}
