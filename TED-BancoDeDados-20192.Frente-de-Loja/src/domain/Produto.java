package domain;

public class Produto {

	private String nomeProduto;
	private Integer id_Produto;
	private String descricao;
	private Double valor;
	private Integer quantidade;

	public Integer codigo;

	public String getNomeProduto() {
		return nomeProduto;
	}

	public void setNomeProduto(String nomeProduto) {
		this.nomeProduto = nomeProduto;
	}

	public Integer getId_Produto() {
		return id_Produto;
	}

	public void setId_Produto(Integer id_Produto) {
		this.id_Produto = id_Produto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

}
