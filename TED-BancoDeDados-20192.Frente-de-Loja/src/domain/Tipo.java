package domain;

public class Tipo {
	private String materia_prima;
	private String nome_tipo;

	public String getMateria_prima() {
		return materia_prima;
	}

	public void setMateria_prima(String materia_prima) {
		this.materia_prima = materia_prima;
	}

	public String getNome_tipo() {
		return nome_tipo;
	}

	public void setNome_tipo(String nome_tipo) {
		this.nome_tipo = nome_tipo;
	}

}
