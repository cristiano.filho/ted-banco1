package domain;

public class Categoria {

	private int codigoCategoria;
	private String descricao;
	private Long id_Categoria;

	public Categoria() {
		this.codigoCategoria = codigoCategoria;
		this.descricao = descricao;
	}

	public int getCodigoCategoria() {
		return codigoCategoria;
	}

	public void setCodigoCategoria(int codigoCategoria) {
		this.codigoCategoria = codigoCategoria;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Long getCodigo() {
		return id_Categoria;
	}

	public void setCodigo(Long codigo) {
		this.id_Categoria = codigo;
	}

}
