package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conectionSQL.ConnectionFactory;
import domain.Categoria;

public class CategoriaDAO {
	Connection con;

	public CategoriaDAO() {
		con = ConnectionFactory.getConnection();
	}

	public void adicionar(Categoria categoria) {
		String sql = "insert into categoria (codigoCategoria, descricao) values (?,?)";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, categoria.getDescricao());
			stmt.setString(2, categoria.getDescricao());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public List<Categoria> listar(Long id_Cargo) {
		List<Categoria> categorias = new ArrayList<>();
		String sql = "Select * from categoria where id_cargo=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setLong(1, id_Cargo);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Categoria categoria = new Categoria();
				categoria.setCodigoCategoria(rs.getInt("codigo"));
				categoria.setDescricao(rs.getString("descri��o"));
				categorias.add(categoria);
			}
			rs.close();
			stmt.close();
			return categorias;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void alterar(Categoria categoria) {
		String sql = "Update candidato set codigo=? descri��o=?  where id_Categoria=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setLong(1, categoria.getCodigoCategoria());
			stmt.setString(2, categoria.getDescricao());
			
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void excluir(Categoria categoria) {
		String sql = "Delete from candidato where id_Categoria=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setLong(1, categoria.getCodigo());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public Categoria getCategoria(Long id_Categoria) {
		String sql = "select * from categoria where id_Categoria=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setLong(1, id_Categoria);
			Categoria categoria = new Categoria();
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				categoria.setCodigoCategoria(rs.getInt("codigo categoria"));
				categoria.setDescricao(rs.getString("descri��o categoria"));
				categoria.setCodigo(id_Categoria);
			}
			rs.close();
			stmt.close();
			return categoria;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
}
