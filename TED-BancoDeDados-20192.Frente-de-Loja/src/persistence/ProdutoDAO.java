package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Produto;

public class ProdutoDAO {
	 Connection con ;

	con=ConnectionFactory.getConnection();

	public ProdutoDAO() {

	}

	public void adicionar(Produto produto) {
		String sql = "insert into produto (nomeProduto,codigoProduto, descricao, valor, quantidade) values (?,?,?,?,?)";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, produto.getNomeProduto());
			stmt.setInt(1, produto.getId_Produto());
			stmt.setString(2, produto.getDescricao());
			stmt.setDouble(3, produto.getValor());
			stmt.setDouble(4, produto.getQuantidade());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			// TODO AackTrace();
			throw new RuntimeException(e);
		}

	}

	public List<Produto> listar(String NomeProduto) {
		List<Produto> produtos = new ArrayList<>();
		String sql = "Select * from produto where NomeProduto=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, NomeProduto);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Produto produto = new Produto();
				produto.setNomeProduto(rs.getString("nome"));
				produto.setValor(rs.getDouble("quantidade"));
				produto.setId_Produto(rs.getInt("id_produto"));
				produto.setDescricao(rs.getString("descricao"));
				produtos.add(produto);
			}
			rs.close();
			stmt.close();
			return produtos;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void alterar(Produto produto) {
		String sql = "Update produto set nomeProduto=?, quantidade=? where id_produto=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, produto.getNomeProduto());
			stmt.setInt(2, produto.getQuantidade());

			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void excluir(Produto produto) {
		String sql = "Delete from produto where id_produto=?;";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setLong(1, produto.getId_Produto());
			stmt.execute();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public Long getId(Produto produto) {
		String sql = "select id_produto from produto where nome=?";
		try {
			PreparedStatement stmt = con.prepareStatement(sql);
			stmt.setString(1, produto.getNomeProduto());
			stmt.setLong(2, produto.getId_Produto());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}
}
