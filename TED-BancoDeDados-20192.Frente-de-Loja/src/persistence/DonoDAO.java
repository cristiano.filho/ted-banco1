package persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import conectionSQL.ConnectionFactory;
import domain.Dono;

public class DonoDAO {
	private Connection connection;

	public DonoDAO() {
		this.connection = ConnectionFactory.getConnection();
	}

	public Dono autenticar(Dono usuario) {
		Dono retorno = null;

		try {
			String sql = "SELECT * FROM usuario where login=? and senha=?";
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, usuario.getLogin());
			preparedStatement.setString(2, usuario.getSenha());

			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet.next()) {
				retorno = new Dono();
				retorno.setLogin(resultSet.getString("login"));
				retorno.setSenha(resultSet.getString("senha"));

				retorno.setId(resultSet.getInt("usuario_id"));
				retorno.setCnpj(resultSet.getString("cnpj"));
				retorno.setEndereco(resultSet.getString("endereco"));

			}

			resultSet.close();
			preparedStatement.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return retorno;
	}

	public void cadastrarUsuario(Dono usuario) {
		String sql = "INSERT INTO USUARIO (LOGIN,SENHA,PAPEL,CNPJ,ENDERECO) VALUES (?,?,?,?,?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, usuario.getLogin());
			preparedStatement.setString(2, usuario.getSenha());

			preparedStatement.setString(4, usuario.getCnpj());
			preparedStatement.setString(5, usuario.getEndereco());
			preparedStatement.execute();
			preparedStatement.close();

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public int getId(Dono usuario) throws SQLException {
		String sql = "SELECT (USUARIO_ID) FROM USUARIO WHERE login =?";
		int retorno = 0;
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setString(1, usuario.getLogin());
		ResultSet resultSet = preparedStatement.executeQuery();
		if (resultSet.next()) {
			retorno = resultSet.getInt("USUARIO_ID");
		}
		return retorno;
	}

}
